# Django tutorial

[![pipeline status](https://gitlab.com/charles2910/django-tutorial/badges/main/pipeline.svg)](https://gitlab.com/charles2910/django-tutorial/-/commits/main)

[![coverage report](https://gitlab.com/charles2910/django-tutorial/badges/main/coverage.svg)](https://gitlab.com/charles2910/django-tutorial/-/commits/main)

This project is based on the [django tutorial](https://docs.djangoproject.com/en/3.1/intro/)
and is a very basic django app that ilustrates the various concepts behind
django, such as basic python packaging, url mangling, app views, templates,
static content, admin page, customization, etc.

This project is licensed under the terms of GPL-3+.
